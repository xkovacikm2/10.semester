(TeX-add-style-hook
 "project"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "slovak" "11pt" "twoside" "openright")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("geometry" "a4paper" "left=1in" "right=1in" "top=1in" "bottom=1in" "footskip=.25in")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "makeidx"
    "geometry"
    "etoolbox")
   (TeX-add-symbols
    "Subtitle"
    "Year"
    "Month"
    "Title"
    "Author")
   (LaTeX-add-labels
    "sec:org8c1d38c"
    "sec:orgad737a1"
    "sec:org57f4ee9"
    "sec:org69c9bc1"
    "sec:org0bbd2f4"
    "sec:orgcaa3611"
    "sec:org9cdfc87"
    "sec:orgc50fbe3"
    "sec:org8e8c1aa"
    "sec:org93ea3a6"
    "sec:org53fa9ce"
    "sec:org4152efb"
    "sec:org517687e"
    "sec:org71e4573"
    "sec:org9bbb632"
    "sec:orgc542a37"
    "sec:org1c2779a"
    "sec:orgfb0fff3"))
 :latex)

