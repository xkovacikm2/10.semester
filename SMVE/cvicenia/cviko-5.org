#+OPTIONS: toc:nil ':t
#+AUTHOR: Michal Kovacik
#+TITLE: Cvicenie 6
* Cviko 5                                                          :noexport:
** parovy t test
*** priklad 1
#+BEGIN_SRC R :results output :exports both :session
x1 <- c(28,35,32,15,35,24,29,19,21,12)
x2 <- c(25,35,30,11,32,28,28,25,22,11)
t.test(x1,x2,paired=TRUE)
#+END_SRC

#+RESULTS:
#+begin_example

	Paired t-test

data:  x1 and x2
t = 0.29656, df = 9, p-value = 0.7735
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -1.988397  2.588397
sample estimates:
mean of the differences 
                    0.3
#+end_example

*** priklad2
zistit, ci sa pri novej geometrii ojazdia gumy rovnako rychlo

#+BEGIN_SRC R :results output :exports none :session
library(readxl)
Auta <- read_excel('Auta.xlsx')
#+END_SRC

#+BEGIN_SRC R :results output :exports both :session
t.test(Auta$PP, Auta$LP, paired=T)
#+END_SRC

#+RESULTS:
#+begin_example

	Paired t-test

data:  Auta$PP and Auta$LP
t = 1.0518, df = 5, p-value = 0.3411
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.1203401  0.2870068
sample estimates:
mean of the differences 
             0.08333333
#+end_example

- vyslo, ze stredna hodnota rozdielu je v intervale spolahlivosti, a p value > ako hladina vyznamnosti
- tento test mozeme pouzit iba ak su data normalne rozdelene

*** priklad 3

#+BEGIN_SRC R :results output :exports none :session
Data_BA <- read_excel('Data_BA.xlsx')
#+END_SRC

- pre bratislavsky kraj testujte rovnost strednych hodnot poctu novorodencov

#+BEGIN_SRC R :results output :exports both :session
x1<-Data_BA$Narodeni[1:9]
x2<-Data_BA$Narodeni[10:18]
t.test(x1, x2, paired=TRUE)
#+END_SRC

#+RESULTS:
#+begin_example

	Paired t-test

data:  x1 and x2
t = -7.645, df = 8, p-value = 6.045e-05
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -1814.3356  -973.4421
sample estimates:
mean of the differences 
              -1393.889
#+end_example

- podla p hodnoty zamietame, lebo je mensia ako 0.05

#+BEGIN_SRC R :results output :exports both :session
t.test(x1, x2, paired = TRUE, alternative = 'greater')
#+END_SRC

#+RESULTS:
#+begin_example

	Paired t-test

data:  x1 and x2
t = -7.645, df = 8, p-value = 1
alternative hypothesis: true difference in means is greater than 0
95 percent confidence interval:
 -1732.935       Inf
sample estimates:
mean of the differences 
              -1393.889
#+end_example

- ukazuje sa, ze po roku 2004 sa rodi signifikatne menej deti

*** priklad 4
- Pre nitrainsky kraj testuj rovnost strednych hodnot migracneho salda do roku 2004 a po roku 2004


#+BEGIN_SRC R :results output :exports none :session
Data_NR <- read_excel('Data_NR.xlsx')
x1 <- Data_NR$Migr_saldo[1:9]
x2 <- Data_NR$Migr_saldo[10:18]
#+END_SRC

#+RESULTS:

#+BEGIN_SRC R :results output :exports both :session
t.test(x1, x2, paired = TRUE)
#+END_SRC

#+RESULTS:
#+begin_example

	Paired t-test

data:  x1 and x2
t = 2.5984, df = 8, p-value = 0.0317
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
  24.72102 414.61231
sample estimates:
mean of the differences 
               219.6667
#+end_example

- je staticky vyznamny rozdiel v migracii pred a po roku 2004 v NR

#+BEGIN_SRC R :results output :exports both :session
t.test(x1, x2, paired = TRUE, alternative = 'greater')
#+END_SRC

#+RESULTS:
#+begin_example

	Paired t-test

data:  x1 and x2
t = 2.5984, df = 8, p-value = 0.01585
alternative hypothesis: true difference in means is greater than 0
95 percent confidence interval:
 62.46366      Inf
sample estimates:
mean of the differences 
               219.6667
#+end_example

- po roku 2004 je staticky vyznamne nizsie migracne saldo v NR

** dvojvyberove paramtericke testy o parametroch normalneho rozdelenia 
*** priklad 1
- overit ci je pocet narodenych v BA statisticky vyznamne nizsi ako v NR

#+BEGIN_SRC R :results output :exports none :session
x <- Data_BA$Narodeni
y <- Data_NR$Narodeni
#+END_SRC

#+RESULTS:

- f test na rovnost rozptylov
- $H0: s^2(x) = s^2(y)$ oproti $H0: s^2(x) \neq s^2(y)$
#+BEGIN_SRC R :results output :exports both :session
var.test(x, y, ratio = 1, alternative = 'two.sided', conf.level = 0.95)
#+END_SRC

#+RESULTS:
#+begin_example

	F test to compare two variances

data:  x and y
F = 246.81, num df = 17, denom df = 17, p-value < 2.2e-16
alternative hypothesis: true ratio of variances is not equal to 1
95 percent confidence interval:
  92.32457 659.80078
sample estimates:
ratio of variances 
          246.8113
#+end_example

- zistili sme, ze rozptyly oboch suborov su statisticky vyznamne odlisne
- spravime t-test strednych hodnoch, ale povieme programu, ze mame rozne rozptyly

#+BEGIN_SRC R :results output :exports both :session
t.test(x, y, alternative = 'greater', var.equal = FALSE)
#+END_SRC

#+RESULTS:
#+begin_example

	Welch Two Sample t-test

data:  x and y
t = 17.337, df = 17.138, p-value = 1.325e-12
alternative hypothesis: true difference in means is greater than 0
95 percent confidence interval:
 2946.041      Inf
sample estimates:
mean of x mean of y 
4071.3889  796.9444
#+end_example

- nezamietame H1, ze v BA je viac narodenych ako v NR

** kovariancia a korelacia
- Zavislost (korelacia) dvoch spojitych nahodnych premennych

*** priklad 1
- x je vek pracovnika
- y je pocet vymeskanych hodin
- zisti korelaciu medzi vekom a poctom vymeskanych hodin

#+BEGIN_SRC R :results output :exports none :session
x <- c(27,61,37,23,46,58,29,36,64,40)
y <- c(15,6,10,18,9,7,14,11,5,8)
#+END_SRC

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig51.png :session
plot(x,y)
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig51.png]]


#+BEGIN_SRC R :results output :exports both :session
cor(x, y, use = 'everything', method = 'pearson')
cor(x, y, use = 'everything', method = 'spearman')
cor(x, y, use = 'everything', method = 'kendall')
#+END_SRC

#+RESULTS:
: [1] -0.9325434
: [1] -0.9878788
: [1] -0.9555556

#+BEGIN_SRC R :results output :exports both :session
library(corrplot)
#+END_SRC

#+BEGIN_SRC R :results output :exports both :session
BA <- subset(Data_BA, select=-c(1))
(cor <- cor(BA))
#+END_SRC

#+RESULTS:
: Error in subset(Data_BA, select = -c(1)) : object 'Data_BA' not found
: Error in is.data.frame(x) : object 'BA' not found

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig52.png :session
corrplot.mixed(cor, lower = 'number', upper = 'circle', tl.pos = 'lt')
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig52.png]]


#+BEGIN_SRC R :results output :exports none :session
Kriminalita <- read_excel('KriminalitaEU.xls')
#+END_SRC

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig54.png :session
plot(Kriminalita$Krdez, Kriminalita$Vlamn)
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig51.png]]

#+BEGIN_SRC R :results output :exports both :session
cor(Kriminalita$Krdez, Kriminalita$Vlamn, method = 'pearson')
cor(Kriminalita$Krdez, Kriminalita$Vlamn, method = 'kendall')
cor(Kriminalita$Krdez, Kriminalita$Vlamn, method = 'spearman')
#+END_SRC

#+RESULTS:
: [1] 0.7283704
: [1] 0.4725464
: [1] 0.6314756

* cviko 6

#+BEGIN_SRC R :results output :exports none :session
x <- c(27,61,37,23,46,58,29,36,64,40)
y <- c(15,6,10,18,9,7,14,11,5,8)
#+END_SRC

#+RESULTS:

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig61.png :session
plot(x,y)
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig61.png]]
- vidime vysoku negativnu korelaciu

#+BEGIN_SRC R :results output :exports both :session
cor(x,y, method = 'kendall')
cor(x,y, method = 'spearman')
cor(x,y, method = 'pearson')
#+END_SRC

#+RESULTS:
: [1] -0.9555556
: [1] -0.9878788
: [1] -0.9325434

- potvrdili sme pozorovanie z grafu, je tu podozrenie na korelaciu

** test na korelaciu

#+BEGIN_SRC R :results output :exports both :session
cor.test(x, y, alternative = 'two.sided', method = 'pearson', conf.level = 0.95)
#+END_SRC

#+RESULTS:
#+begin_example

	Pearson's product-moment correlation

data:  x and y
t = -7.3053, df = 8, p-value = 8.346e-05
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 -0.9842585 -0.7337288
sample estimates:
       cor 
-0.9325434
#+end_example

- zamietame hypotezu, ze data niesu korelovane
- teda existuje korelacia medzi vekom a poctom dni volna
- 
#+BEGIN_SRC R :results output :exports both :session
cor.test(x, y, alternative = 'two.sided', method = 'spearman', conf.level = 0.95)
#+END_SRC

#+RESULTS:
: 
: 	Spearman's rank correlation rho
: 
: data:  x and y
: S = 328, p-value < 2.2e-16
: alternative hypothesis: true rho is not equal to 0
: sample estimates:
:        rho 
: -0.9878788

- spearman je vhodnejsi pre vzorky, ktore nemaju normalne rozdelenie

#+BEGIN_SRC R :results output :exports both :session
cor.test(x, y, alternative = 'two.sided', method = 'kendall', conf.level = 0.95)
#+END_SRC

#+RESULTS:
: 
: 	Kendall's rank correlation tau
: 
: data:  x and y
: T = 1, p-value = 5.511e-06
: alternative hypothesis: true tau is not equal to 0
: sample estimates:
:        tau 
: -0.9555556

** linearna regresia
*** jednovyberovy subor
- sledujem jednu datovu vzorku (robotnici vo fabrike)
- regresia sa da robit len na jednovyberovom subore (nemozem jeden atribut sledovat v jednej fabrike a iny v druhej)

#+BEGIN_SRC R :results output :exports both :session
xm <- mean(x); xm %vek
#+END_SRC

#+RESULTS:
: [1] 42.1

#+BEGIN_SRC R :results output :exports both :session
ym <- mean(y); ym %absencie
#+END_SRC

#+RESULTS:
: [1] 10.3

#+BEGIN_SRC R :results output :exports both :session
a <- sum((x-xm)*(y-ym))/sum((x-xm)^2); a
#+END_SRC

#+RESULTS:
: [1] -0.2681088
- a - smernica prelozenej priamky, o kolko sa zmeni y ked x zmenim o 1

#+BEGIN_SRC R :results output :exports both :session
b <- ym - a*xm; b
#+END_SRC

#+RESULTS:
: [1] 21.58738
- b - konstanta, posun priamky pre x = 0
- $y=21,59 - 0,27x$

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig62.png :session
plot(x,y)
abline(b,a)
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig62.png]]


#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig63.png :session
scatter.smooth(x,y, main = "Pocet dni absencie ~ Vek") #vztah
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig63.png]]


#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig64.png :session
par(mfrow=c(1,2)) # 1 riadok a 2 stlpce
boxplot(x, main = "Vek", sub = paste("Outlier rows: ", boxplot.stats(x)$out)) # boxplot pre vek
boxplot(y, main = "Pocet dni absencie", sub = paste("Outlier rows: ", boxplot.stats(y)$out))
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig64.png]]

- vyzera to na normalne rozdelenie

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig65.png :session
library(e1071)
par(mfrow=c(1,2)) # 1 riadok a 2 stlpce
plot(density(x), main = "Hustota: vek", ylab="", sub = paste("Sikmost:", round(e1071::skewness(x), 2)))
polygon(density(x), col = "grey")
plot(density(y), main = "Hustota: Pocet volnych dni", ylab="", sub = paste("Sikmost:", round(e1071::skewness(y), 2)))
polygon(density(y), col = "grey")
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig65.png]]

#+BEGIN_SRC R :results output :exports both :session
linMod <- lm(y ~ x)
linMod
#+END_SRC

#+RESULTS:
: 
: Call:
: lm(formula = y ~ x)
: 
: Coefficients:
: (Intercept)            x  
:     21.5874      -0.2681

#+BEGIN_SRC R :results output :exports both :session
summary(linMod)
#+END_SRC

#+RESULTS:
#+begin_example

Call:
lm(formula = y ~ x)

Residuals:
    Min      1Q  Median      3Q     Max 
-2.8630 -0.7652  0.3797  0.7383  2.5791 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)  21.5874     1.6273  13.265 9.95e-07 ***
x            -0.2681     0.0367  -7.305 8.35e-05 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 1.615 on 8 degrees of freedom
Multiple R-squared:  0.8696,	Adjusted R-squared:  0.8533 
F-statistic: 53.37 on 1 and 8 DF,  p-value: 8.346e-05
#+end_example

- *residual* - rozdiel medzi namerou hodnotou a predikovanou hodnotou
- *Pr(>|t|)* - ukazuje vyznamnost parametra pre model
- *Multiple R-squared* - chcem co najblizsie k 1

#+BEGIN_SRC R :results output :exports both :session
(linAIC <- AIC(linMod))
(linBIC <- BIC(linMod))
#+END_SRC

#+RESULTS:
: [1] 41.73657
: [1] 42.64432

- vypocet informacneho kriteria

#+BEGIN_SRC R :results output :exports both :session
confint(linMod)
#+END_SRC

#+RESULTS:
:                  2.5 %    97.5 %
: (Intercept) 17.8347430 25.340021
: x           -0.3527407 -0.183477

- ukazuje, ze s pravdepodobnostou 95% budu koeficienty v tychto intevaloch


- tieto niesu dolezite

#+BEGIN_SRC R :results output :exports both :session
(predik.int <- predict(linMod, data.frame(x=x), interval = 'confidence', level = 0.99))
#+END_SRC

#+RESULTS:
#+begin_example
         fit       lwr       upr
1  14.348443 11.819621 16.877266
2   5.232743  2.342376  8.123110
3  11.667355  9.842066 13.492644
4  15.420879 12.510643 18.331114
5   9.254376  7.474516 11.034235
6   6.037070  3.434950  8.639189
7  13.812226 11.458578 16.165873
8  11.935464 10.064228 13.806700
9   4.428417  1.233049  7.623784
10 10.863029  9.129788 12.596269
#+end_example

#+BEGIN_SRC R :results output :exports both :session
(predik.int <- predict(linMod, data.frame(x=x), interval = 'predict', level = 0.99))
#+END_SRC

#+RESULTS:
#+begin_example
         fit        lwr      upr
1  14.348443  8.3678579 20.32903
2   5.232743 -0.9094617 11.37495
3  11.667355  5.9486018 17.38611
4  15.420879  9.2692992 21.57246
5   9.254376  3.5499597 14.95879
6   6.037070  0.0251242 12.04901
7  13.812226  7.9035782 19.72087
8  11.935464  6.2018801 17.66905
9   4.428417 -1.8630726 10.71991
10 10.863029  5.1729862 16.55307
#+end_example

*** kvadraticka forma
#+BEGIN_SRC R :results output :exports both :session
linMod2 <- lm( y ~ poly(x,2))
linMod2
#+END_SRC

#+RESULTS:
: 
: Call:
: lm(formula = y ~ poly(x, 2))
: 
: Coefficients:
: (Intercept)  poly(x, 2)1  poly(x, 2)2  
:      10.300      -11.800        3.812

#+BEGIN_SRC R :results output :exports both :session
summary(linMod2)
#+END_SRC

#+RESULTS:
#+begin_example

Call:
lm(formula = y ~ poly(x, 2))

Residuals:
     Min       1Q   Median       3Q      Max 
-1.42252 -0.41171  0.01061  0.44503  1.30543 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)  10.3000     0.3009  34.228 4.71e-09 ***
poly(x, 2)1 -11.7995     0.9516 -12.400 5.10e-06 ***
poly(x, 2)2   3.8121     0.9516   4.006  0.00515 ** 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.9516 on 7 degrees of freedom
Multiple R-squared:  0.9604,	Adjusted R-squared:  0.9491 
F-statistic:  84.9 on 2 and 7 DF,  p-value: 1.235e-05
#+end_example

- model sa zlepsil, lebo ma uzsie rezidualy
- novo pridany atribut je statisticky vyznamny
- MRS je blizsie k 1

#+BEGIN_SRC R :results output :exports both :session
(linAIC <- AIC(linMod2))
(linBIC <- BIC(linMod2))
#+END_SRC

#+RESULTS:
: [1] 31.82005
: [1] 33.03039

- informacne kriterium je lepsie -> lepsi model

#+BEGIN_SRC R :results output :exports both :session
confint(linMod2)
#+END_SRC

#+RESULTS:
:                  2.5 %    97.5 %
: (Intercept)   9.588423 11.011577
: poly(x, 2)1 -14.049734 -9.549327
: poly(x, 2)2   1.561900  6.062307


#+BEGIN_SRC R :results output :exports both :session
predict(linMod2, data.frame(x=mean(x)), interval="confidence", level=0.95)
#+END_SRC

#+RESULTS:
:        fit      lwr      upr
: 1 8.752298 7.594299 9.910297

- hovori strednu hodnotu pre x

** domaca uloha
- u 15tich 20 rocnych studentov bola zistovana telesna vyska a hmotnost, udaje su v tabulke. Nakreslite bodovy graf vysky a hmotnosti. Zistite, ci vyska a hmotnost su zavisle premenne. Zavislost otestuj na hl. vyznamnosti 0.05. Vypocitajte model zavislosti hmotnosti od vysky. Testuje vyznamnost koeficientov. Vypocitajte intervaly spolahlivosti pre koeficienty regresnej primaky. Vypocitajte predpovedane hodnoty a rezidua. Porovnajte vysledkych dvoch regresnych modelov a vyberte vhodnejsie.
