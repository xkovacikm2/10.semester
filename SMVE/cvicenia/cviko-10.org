* cviko-10
** test podobnosti
#+BEGIN_SRC R :results output :exports none :session
library(readxl)

pabc <- read_excel("KW.xlsx")
pabc$Univerzita <- factor(pabc$Univerzita)
#+END_SRC

Zisti ci existuje statisticky vyznamny rozdiel medzi poctami absolventov
bakalarskeho studia

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig101.png :session
boxplot(pabc$PABC ~ pabc$Univerzita)
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig101.png]]

#+BEGIN_SRC R :results output :exports both :session
library(ggplot2)
#+END_SRC

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig102.png :session
ggplot(pabc, aes(x = Univerzita, y = PABC, color = Univerzita)) + geom_boxplot()
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig102.png]]

spravime kruskalov test -> neparametricke porovnanie strednych hodnot

#+BEGIN_SRC R :results output :exports both :session
kruskal.test(pabc$PABC ~ pabc$Univerzita)
#+END_SRC

#+RESULTS:
: 
: 	Kruskal-Wallis rank sum test
: 
: data:  pabc$PABC by pabc$Univerzita
: Kruskal-Wallis chi-squared = 55.149, df = 6, p-value = 4.324e-10

zamietame hypotezu, ze neexistuje statisticky vyznamny rozdiel medzi strednymi hodnotami

#+BEGIN_SRC R :results output :exports both :session
library(FSA)
#+END_SRC

testujeme kontrasty, lebo sme zamietli kruskala. post-hoc test, mnohonasobne
porovnavanie strednych hodnot medzi univerzitami
#+BEGIN_SRC R :results output :exports both :session
(d <- dunnTest(pabc$PABC ~ pabc$Univerzita, method = 'bh'))
#+END_SRC

#+RESULTS:
#+begin_example
Dunn (1964) Kruskal-Wallis multiple comparison
  p-values adjusted with the Benjamini-Hochberg method.

     Comparison          Z      P.unadj        P.adj
1    ČVUT - STU  1.7230541 8.487875e-02 1.273181e-01
2   ČVUT - TUKE  1.2730026 2.030171e-01 2.507858e-01
3    STU - TUKE -0.4500514 6.526734e-01 6.526734e-01
4    ČVUT - TUL  4.1276146 3.665458e-05 1.924365e-04
5     STU - TUL  2.4045605 1.619192e-02 3.091186e-02
6    TUKE - TUL  2.8546120 4.308944e-03 1.005420e-02
7   ČVUT - TUZA  2.8546120 4.308944e-03 1.131098e-02
8    STU - TUZA  1.1315579 2.578203e-01 3.007904e-01
9   TUKE - TUZA  1.5816093 1.137388e-01 1.592343e-01
10   TUL - TUZA -1.2730026 2.030171e-01 2.664600e-01
11 ČVUT - VŠCHT  5.1820208 2.194947e-07 2.304694e-06
12  STU - VŠCHT  3.4589668 5.422518e-04 1.626755e-03
13 TUKE - VŠCHT  3.9090182 9.267198e-05 3.892223e-04
14  TUL - VŠCHT  1.0544062 2.916970e-01 3.224019e-01
15 TUZA - VŠCHT  2.3274089 1.994351e-02 3.490115e-02
16   ČVUT - VUT -0.7586581 4.480571e-01 4.704599e-01
17    STU - VUT -2.4817122 1.307528e-02 2.745809e-02
18   TUKE - VUT -2.0316608 4.218801e-02 6.814986e-02
19    TUL - VUT -4.8862727 1.027628e-06 7.193397e-06
20   TUZA - VUT -3.6132701 3.023594e-04 1.058258e-03
21  VŠCHT - VUT -5.9406790 2.838440e-09 5.960724e-08
#+end_example

vyfiltrujem si iba tie riadky, ktore su statisticky vyznamne

#+BEGIN_SRC R :results output :exports both :session
my.d <- data.frame(Universities = d$res$Comparison, p.val = d$res$P.unadj)
my.d[my.d$p.val < 0.05, ]
#+END_SRC

#+RESULTS:
#+begin_example
   Universities        p.val
4    ČVUT - TUL 3.665458e-05
5     STU - TUL 1.619192e-02
6    TUKE - TUL 4.308944e-03
7   ČVUT - TUZA 4.308944e-03
11 ČVUT - VŠCHT 2.194947e-07
12  STU - VŠCHT 5.422518e-04
13 TUKE - VŠCHT 9.267198e-05
15 TUZA - VŠCHT 1.994351e-02
17    STU - VUT 1.307528e-02
18   TUKE - VUT 4.218801e-02
19    TUL - VUT 1.027628e-06
20   TUZA - VUT 3.023594e-04
21  VŠCHT - VUT 2.838440e-09
#+end_example

** testy dobrej zhody
zisti ake je tam rozdelenie v oboch datasetoch

#+BEGIN_SRC R :results output :exports both :session
zs <- read_excel("Test.xlsx", sheet = 1)
zs$Sposob <- factor(zs$Sposob)

gym <- read_excel("Test.xlsx", sheet = 2)
gym$Sposob <- factor(gym$Sposob)
#+END_SRC


#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig103.png :session
ggplot(zs, aes(x = Sposob, y = Body, color = Sposob)) + geom_boxplot()
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig103.png]]

#+BEGIN_SRC R :results output graphics :exports results :file //home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig104.png :session
ggplot(gym, aes(x = Sposob, y = Body, color = Sposob)) + geom_boxplot()
#+END_SRC

#+RESULTS:
[[file://home/kovko/Documents/FIIT/ING/2.semester/SMVE/cvicenia/figures/fig104.png]]

otestujeme ci su data z normalneho vyberu

#+BEGIN_SRC R :results output :exports both :session
zs.prac = subset(zs, zs$Sposob == "prac_list")
shapiro.test(zs.prac$Body)
#+END_SRC

#+RESULTS:
: 
: 	Shapiro-Wilk normality test
: 
: data:  zs.prac$Body
: W = 0.92225, p-value = 0.1417

body po prac_zosite na zs su normalne rozdelene

#+BEGIN_SRC R :results output :exports both :session
zs.present = subset(zs, zs$Sposob == "prezentacia")
shapiro.test(zs.present$Body)
#+END_SRC

#+RESULTS:
: 
: 	Shapiro-Wilk normality test
: 
: data:  zs.present$Body
: W = 0.90716, p-value = 0.03561

po prezentacii niesu body normalne rozdelene

#+BEGIN_SRC R :results output :exports both :session
gym.prac = subset(gym, gym$Sposob == "prac_list")
shapiro.test(gym.prac$Body)
#+END_SRC

#+RESULTS:
: 
: 	Shapiro-Wilk normality test
: 
: data:  gym.prac$Body
: W = 0.87203, p-value = 0.08233

#+BEGIN_SRC R :results output :exports both :session
gym.present = subset(gym, gym$Sposob == "prezentacia")
shapiro.test(gym.present$Body)
#+END_SRC

#+RESULTS:
: 
: 	Shapiro-Wilk normality test
: 
: data:  gym.present$Body
: W = 0.94143, p-value = 0.3355

na gympli su obe bodovania z normalneho rozdelenia

pre zs musim pouzit neparametricky test

#+BEGIN_SRC R :results output :exports both :session
kruskal.test(zs$Body ~ zs$Sposob)
#+END_SRC

#+RESULTS:
: 
: 	Kruskal-Wallis rank sum test
: 
: data:  zs$Body by zs$Sposob
: Kruskal-Wallis chi-squared = 5.3837, df = 1, p-value = 0.02033

existuje statisticky vyznamny rozdiel medzi metodami

na gympli mozem pouzit anovu
#+BEGIN_SRC R :results output :exports both :session
res.aov <- aov(Body ~ Sposob, data = gym)
summary(res.aov)
#+END_SRC

#+RESULTS:
:             Df Sum Sq Mean Sq F value  Pr(>F)   
: Sposob       1  31.02  31.018   8.584 0.00697 **
: Residuals   26  93.95   3.613                   
: ---
: Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

existuje statisticky vyznamny rozdiel medzi metodami
