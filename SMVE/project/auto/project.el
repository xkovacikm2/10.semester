(TeX-add-style-hook
 "project"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "slovak" "11pt" "twoside" "openright")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("geometry" "a4paper" "left=1in" "right=1in" "top=1in" "bottom=1in" "footskip=.25in")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "makeidx"
    "geometry"
    "etoolbox")
   (TeX-add-symbols
    "Subtitle"
    "Year"
    "Month"
    "Title"
    "Author")
   (LaTeX-add-labels
    "sec:orgfd5cd4d"
    "sec:org45fc656"
    "sec:org67224ab"
    "sec:orgc7deae1"
    "sec:orgdd4cc43"
    "sec:org14198ee"
    "sec:org7955b6c"
    "sec:orgfee68b0"
    "sec:org3c8c0c4"
    "sec:org36a9ee7"
    "sec:orgf218008"
    "sec:org4bfd7da"
    "sec:orge1e3fbb"
    "sec:orgb83e4ef"
    "sec:orga92c7bc"
    "sec:org0e553b0"))
 :latex)

