* Prednaska 1
- v pondelok o 14:00 I32 na FMFI (informaticky pavilon)
- v piatok miestnost I9 na FMFI
- 7x tam a 4x na FIITke na PRPR cvika
- robit si poznamky z cvik, ze ake boli hiccups
- budeme mat pristup do peoplie a pozerat co robia
- treba si pisat *denniky* pocas seminara
* Prednaska 2
- prva motivacia ucit sa vznika v rodine
- ako skusal oblubeny profesor
  - posadil vela studentov, dal im problem a o 2 hodiny sa s nimi porozpraval a rozdal znamky
  - perfektny sposob ako vyskusat inzinierske citenie
  - nechat studentov riesit problem samych, nevstupovat im do procesu, neukazovat im riesenie
- Montesori - poznatok nemozno odovzdat, ale mozno vytvorit prostredie pre objavenie poznatku
- treba ziskat vnutornu motivaciu u studenta
- programovanie ako nastroj na objavovanie
- programovanie je specifikovanie spravania objektu v buducnosti, ktore mozem v case modifikovat

** obdobia ucenia
- male dieta sa uci ohmatavanim a postupne abstrahuje
- podobny proces treba absolvovat aj ako dospely
