* charakteristika dobreho ucitela
- *najviac naucil* - Hrivnakova, tvrda, prisna, spravodliva ale aj ludska
- *mal som najradsej* - Chren, diskusie, sarkazmus

** Aktivita - idealny ucitel
*** mal by:
- rozumiet problematike
- ludsky/empaticky
- mal by ho bavit predmet co uci

*** nemal by byt
- naladovy, ustipacny

** osobnostne charakteristiky ucitela
- primerane posudenie reality pri kritickych situaciach a hodnoteni
- usielie o sebapoznanie, zvysena emptia k inym ,zavadzanie pozitivnych zmien v cinnosti
- schopnost ovladat svoje konanie
- tvorivost a iniciativa, byt tvorivy v aktivitach, strure hodiny, rozvijat tvorivost aj u ziakov
- schopnost vytvarat pozitivne emocioinalne vazby, emocionalna inteligencia
- moralne naroky na osobnost ucitela, obzvlast pre mladsich ziakov
- odolnost voci zatazi, ucitelsky stres, syndrom vyhorenia
- zmysel pre humor a optimizmus, zlepsuje klimu v trede
- schopnost optimalnej komunikacie, odraza jeho naladu a vztah k ziakom

* priprava na hodinu
** vyucbovy ciel
*** vseobecne dlhodobe ciele
- definuju konecnu kvalitu
- vyjadrenie zamerov vzdelavania
- popisuju vacsi objem latky
- pomahaju pri zaverecnom hodnoteni
- popisuje co ziak ma vediet/robit 

*** specificke ciastkove ciele
- odvodzovane zo vseobecnych cielov

*** rozdelenie cielov podla 3 domen
**** afektivna (citova) domena
- prejavy emocialneho spravania (praca v skupine)
  - ucta k spoluobcanom
  - poctivost
  - samostatna praca bez rusenia

**** psychomotorickej (pohybovej) domene
- zmysloven ucenie ako tanec, futbal
- mechanicke cinnosti
  - volejbalove podanie
  - mapa sa legendou a sietou suradnic
  - vytvorenie modelu objektu
  - plavanie

**** kognitivnej (rozumovej) oblasti
- pamatanie si informacie
- zaradenie si do suvislosti
  - dolezitost zloziek potravy
  - rozvijanie hollych viet

* ciele pre PRPR
** kognitivne
*** vseobecne
- student rozumie pojmom a vie ich pouzivat v praxi: premenna, cyklus, pole, smernik a praca s pamatou, funkcie, modularne programovanie
- student vie algoritmicky mysliet
- student vie splnit jednoduchu programatorsky ulohu pomocou jazyka C
*** ciastkove
- naucit sa praci MS visual studio, vytvorit projekt a skompilovat ho


** psychomotoricke/afektivne mix
*** vseobecne
- student vie rozumne rozdelit program do zmysluplpnych casti
- student na miestach kde je to vhodne umiestnuje strucne komentare
- student vie pekne formatovat kod
- student vie pracovat samostatne a nevytvara plagiaty
*** ciastkove
- student sa oboznami s pracou kompilatora

* praca s ucivom 
- didakticka analyza uciva
  - hlbsie porozumienie obsahu a strukture
  - uvedomit si skladbu a najst komponenty
- vyucovanie ma strukturu

** fakty pojmy generalizacie
*** pojmy
- kategoriew, alebo triedy veci ci myslienok, ktore maju spolocne najdolezitejsie vlastnosti
- su zakladnou stavebnou jednotkou
- pojmy, ktore ste sa naucili
- pojmova mapa = pomocka
- priklady: cyklus, funkcia, netiketa

*** fakty
- prehlasenia o konkretnych objektoch
- vyjadruju sa vyrokom
- daju sa overit pozorovanim
