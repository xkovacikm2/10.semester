* prednaska 3
** bloomova taxonomia
- urovne vzdelavania

*** Naucenie sa pojmov
- najnizsia uroven
- mam iba naucene co mi hovoril ucitel
*** Porozumenie
- nizsia uroven
- viem vlastnymi slovami porozpravat
*** Aplikacia
- viem nieco pouzit v praxi
- len vyuzijem co som sa naucil
*** Analyza
- aktivna praca s poznatkami
- zlozitejsi myslienkovy proces
*** Synteza
- musim vytvorit nieco originalne/nove
*** Hodnotenie
- viem si vytvorit svoj usudok na nieco
