(TeX-add-style-hook
 "02-prednaska"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:org0bffe98"
    "sec:orgad31e5a"
    "sec:org788fff4"
    "sec:org104b496"
    "sec:org4a1f135"
    "sec:orgff5dcc3"
    "sec:org27031eb"
    "sec:orgee1b02e"
    "sec:orga558add"
    "sec:orgb5ea515"
    "sec:orgc1c400e"
    "sec:orge0cdaa7"
    "sec:org0b06813"
    "sec:org787caba"
    "sec:org5ee15c2"
    "sec:orge02acc2"
    "sec:orgc1c615b"
    "sec:org8756d76"
    "sec:org5e40be3"
    "sec:orgfda4172"))
 :latex)

