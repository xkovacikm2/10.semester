#+LATEX_HEADER: \renewcommand*{\contentsname}{Obsah}
#+AUTHOR: Michal Kovacik
#+TITLE: Didaktický výstup - smerníky
* Ciele vyučovania
** Všeobecné ciele
- Študenti chápu akým spôsobom je prideľovaná pamäť programu.
- Študenti vedia rozdiel medzi stackom a heapom.
- Študenti vedia dynamicky alokovať vopred neznáme množstvo pamäte a vedia s ňou bezpečne pracovať.
** Čiastkové ciele
- Študent vie rozlíšiť, kedy do funkcie posiela adresu a kedy hodnotu premennej.
- Študent vie ako pristúpiť ku hodnote premennej, keď k má k dispozícií jej adresu a ako s ňou pracovať.
- Študent vie používať výstupné parametre pri volaní funkcie.

* Očakávané vedomosti
- Študent vie čo je to premenná a pole a pozná typovanie premenných.
- Študent dokáže deklarovať, zapisovať a volať funkcie.
- Študent vie čítať dáta zo štandardného vstupu.
* Brunnerov koncept
** Pojmy
heap, stack, alokácia, adresa, hodnota, premenná, smerník na hodnotu, smerník na
smerník, smerníkový aritmetika

** Fakty
- Volaním funkcie malloc pridelí operačný systém programu pamäť.
- Volaním funkcie free program vráti nepotrebnú pamäť späť operačnému systému.
- Memory leak je spôsobovaný nesprávnou prácou s pamäťou.
- Dynamická alokácia pamäte prebieha na heape počas behu programu.
- Statická alokácia pamäte prebieha na stacku pri spustení programu.
- Posielaním adresy premennej ju viem modifikovať vo funkcií.

** Generalizácie
- Keď sa chcem vyhnúť memory leaku, je najlepšie pracovať so statickou pamäťou.
- Keď sa snažím pracovať s pamäťou, ktorá mi nepatrí, spôsobím segmentation fault.

#+CAPTION: Pojmová mapa
#+NAME: fig:mapa
#+ATTR_LATEX: :width .99\linewidth
[[file:mapa_pojmov.png]]

* Proces výučby
** Úvodné slovo
Určite ste si všimli, že keď načítavame vstup od používateľa prostredníctvom
funkcie /scanf/ tak musíme poslať do funkcie aj premenné do ktorých chceme
načítavať hodnoty. Navyše sa pred tieto premenné vkladal znak /&/. Keď ste ho
zabudli napísať, tak sa vám stalo, že program nefungoval.

Vieme, že funkcia môže vracať nanajvýš 1 návratovú hodnotu. Avšak mi môžeme chcieť
naraz načítať 3-4 alebo aj viac vecí. Tento problém sa dá obísť prostredníctvom
tzv. výstupných parametrov. To znamená, že funkcií pošlem premenné, ktoré si
želám, aby mi naplnila hodnotami. Keď pred premennú vložíme znak /&/, tak si
od nej pýtame miesto hodnoty, jej adresu. Čo je to adresa?

Pamäť počítača si môžeme predstaviť ako panelák. Je tvorený bitmi (sic) a tie
majú svojich obyvateľov, to sú programy. Kto vlastní ktorý byt, určuje správca - 
operačný systém. Každý takýto byt je tvorený dvoma izbami. Jedna sa volá *stack*,
tam bývajú stáli obyvatelia bytu a tá druhá sa volá *heap*, to je taká hosťovská izba.

Stáli obyvatelia sú takí, ktorí v byte bývajú od spustenia programu, po jeho koniec.
To sú bežné statické a globálne premenné, s ktorými pracujete. Potom sú ale také, ktoré
sú v pamäti iba chvíľu, a to sú napríklad volania funkcií, alebo premenné vo funkciách.

Každý takýto obyvateľ má svoju posteľ. V tejto posteli sa nachádza do nej uložená hodnota.
Keď si pýtam od premennej adresu, tak sa dozviem, že na ktorom poschodí, v ktorom byte a
v ktorej izbe sa nachádza posteľ, v ktorej je hodnota. *Adresa* je teda informácia
o /polohe/ a *hodnota* je informácia o /obsahu/.

Čo sa teda deje pri volaní funkcie /scanf/ je to, že ja jej pošlem adresu do pamäte,
kde očakávam, že po jej ukončení nájdem uložené požadované hodnoty. Na prístup k
adrese premennej sa používa unárny operátor *ampersand*.

** Úloha
*Porozumenie* - cieľom je objasniť rozdiel medzi hodnotou a adresou

Napíšte program, ktorý má 1 premennú typu char a jednu typu double. Obe inicializujte
na ľubovoľnú hodnotu. Vypíšte hodnotu a adresu oboch premenných. Potom do nich načítajte
vstup zo štandardného vstupu a opäť vypíšte hodnoty a adresy oboch premenných.
Pozorujte zmeny vo výpisoch.
#+BEGIN_EXAMPLE
Ukážkový vstup:
q 12.12↵
#+END_EXAMPLE

#+BEGIN_EXAMPLE
Ukážkový výstup:
c: j (0x7fffac467b3e), d: 42.42 (0x7fffac467b3f)
c: q (0x7fffac467b3e), d: 12.12 (0x7fffac467b3f)
#+END_EXAMPLE

** Smerník
Existuje špeciálny typ premennej, ktorá sa nazýva smerník. Jej úlohou nie je
pamätať si hodnotu, ktorá by sa do nej dala uložíť, ako to je pri bežných
premenných. Namiesto toho si pamätá adresu v pamäti, kde sa nachádza hodnota.
Slúži teda ako premenná typu adresa. A je potom už len na programátorovi, aby si
pamätal, čo sa na tej adrese skrýva za hodnotu.

Na prístup ku hodnote, na ktorej adresu ukazuje smerník, sa používa unárny
operátor *hviezdička*.

** Úloha
*Aplikácia* - úloha vyžaduje využiť koncept výstupných parametrov funkcie v praxi.

Napíšte program na výpočet obsahu a obvodu obdĺžnika. Vytvorte funkciu
/nacitaj/, ktorá načíta a pomocou argumentov vráti dve reálne čísla. Ďalej
vytvorte funkciu /obsahObvod/, ktoré ako argumenty dostanú dĺžky strán,
obsah a obvod obdĺžnika a vracia veľkosť uhlopriečky. V hlavnej funkcií volajte
funkciu /nacitaj/ na načítanie rozmerov obdĺžnika a potom volajte funkciu
/obsahObvod/ a nakoniec funkciu /vypis/, ktorá vypočítané atribúty
obdĺžníka vypíše a zaokrúhli na 2 desatinné miesta.
#+BEGIN_EXAMPLE
Ukážkový vstup:
3.5 4.75↵
#+END_EXAMPLE

#+BEGIN_EXAMPLE
Ukážkový výstup:
obsah: 16.625↵
obvod: 16.5↵
uhlopriecka: 5.90↵
#+END_EXAMPLE

** Smerníková aritmetika
Smerníková aritmetika je niečo, s čím ste sa už nevedomky stretli. Každý už
použil pole. Keď v jazyku c zadeklarujem pole, tak na pozadí vlastne dostanem
smerník na prvý prvok v poli. Posun na každý ďalší prvok poľa znamená výpočet
adresy v pamäti.

#+BEGIN_EXAMPLE c
int pole[5];

for(int i = 0; i < 5; i++;){
  pole[i] = i;
}
#+END_EXAMPLE

Takýto zápis všetci poznáte. Na pozadí sa však deje toto:

#+BEGIN_EXAMPLE c
int pole[5];

for(int i = 0; i < 5; i++;){
  *(pole + i) = i;
}
#+END_EXAMPLE

Pre každé políčko v poli vypočítam adresu tak, že pristúpim k prvému prvku,
a k nemu pripočítam offset, teda o koľko miest sa musím posunúť, aby som sa
dostal na želané miesto. Takémuto výpočtu sa hovorí *smerníková aritmetika*.

** Úloha
*Porozumenie* - Úplne jednoduché využitie smerníkovej aritmetiky.

Napíšte program, ktorý zo štandardného vstupu načíta slovo (max. dĺžky 10) a vypíše
ho odzadu. (Využite ukazovateľovú aritmetiku.)
#+BEGIN_EXAMPLE
Ukážka vstupu:
ahoj
#+END_EXAMPLE

#+BEGIN_EXAMPLE
Ukážka výstupu:
joha
#+END_EXAMPLE

** Dynamická alokácia pamäte
Dynamická alokácia pamäte znamená, že program počas svojho behu, vyžiada
od operačného systému ďalšiu pamäť, aby sme si mali kam ukladať dáta.  
K tejto pamäti sa nedá pristupovať cez bežné premenné, ako ste na to zvyknutí,
ale iba cez smerníky, ktoré ukazujú, kde sa takto alokovaná pamäť nachádza.

Na vyžiadanie takejto pamäte, treba operačnému systému povedať koľko jej
potrebujem a on mi ju potom pridelí. Jedna z možností ako na to, je použiť
príkaz /malloc/. Preskúmajme deklaráciu funkcie malloc:

#+BEGIN_EXAMPLE c
void *malloc(size_t size)
#+END_EXAMPLE

Vidíme že berie jeden argument typu veľkosť. Týmto argumentom vieme povedať
koľko pamäti od operačného systému požadujeme. Väčšinou chceme veľkosť nejakého
konkrétneho typu, alebo jej násobok. Pre zistenie veľkosti konkrétneho typu
existuje funkcia /sizeof/, ktorá vracia počet základných jednotiek potrebných
pre uloženie daného typu. V jazyku C je takouto základnou jednotkou jeden
/char/. 

Všimnite si návratový typ funkcie. /void*/ znamená smerník na pamäť bez
konkrétneho typu. Pre použitie v programe si ju musím pretypovať na mnou
požadovaný typ. Príklad:

#+BEGIN_EXAMPLE c
char* c   = (char*)malloc(sizeof(char)); //deklarácia 1 charu
char* arr = (char*)malloc(sizeof(char) * 5); //deklarácia poľa charov veľkosti 5
#+END_EXAMPLE

Každú pamäť, o ktorú som kedy požiadal, musím uvoľniť, keď už ju nepotrebujem.
Pokiaľ by som ju uvoľniť zabudol, a môj program by od operačného systému pýtal
stále ďalšiu pamäť, môže dôjsť k tomu, že počítaču sa minie pamäť a zamrzne.
Stavu, keď program nevracia nepotrebnú pamäť sa hovorí /memory leak/.

Vyžiadanú pamäť vie vrátiť volaním funkcie /free/, ktorá berie ako argument
adresu začiatku pamäti, ktorú chcem uvoľniť. 

#+BEGIN_EXAMPLE c
free(c);
free(arr);
#+END_EXAMPLE

** Úloha
*Aplikácia* rozšírenie predchádzajúcej úlohy o dynamickú alokáciu.

Napíšte program, ktorý z prvého riadku vstupu načíta celé číslo n a alokuje v pamäti
blok n položiek pre znaky. Potom zo štandardného vstupu načíta n znakov a vypíše
ich odzadu. (Využite ukazovateľovú aritmetiku.)
#+BEGIN_EXAMPLE
Ukážka vstupu:
4
ahoj
#+END_EXAMPLE

#+BEGIN_EXAMPLE
Ukážka výstupu:
joha
#+END_EXAMPLE

** Smerník na smerník
Keďže viem do smerníka uložíť adresu inej premennej a keďže smerník je tiež
premenná, znamená to, že môžem vytvoriť aj smerník, ktorý ukazuje na smerník.
Takáto meta úroveň je výhodná, keď potrebujem adresovať polia, najmä tie
viacrozmerné. Napríklad pre dvojrozmerné pole to funguje potom tak, že mám v
smerníku adresu na začiatok poľa smerníkov, ktoré ukazujú na pole hodnôt.

Je to ako keď sa niekoho opýtate, kde je v Inchebe kancelária na 2. poschodí 4.
vľavo a on vás pošle za svojim známym, ktorý vám povie kde je Incheba a potom až
môžete ísť do Incheby a tam sa spýtate vrátnika ako sa dostanete na 2. poschodie,
a potom si už len odrátate správne dvere.

** Úloha
*Aplikácia* - úloha podobná predchádzajúcej, ale s rozšírením do viacerých
rozmerov.

Vytvorte program, ktorý priať používateľovi pekný deň s použitím celého používateľovho
mena. Najprv zo štandardného vstupu načítajte počet mien používateľa. Potom pre každé
meno načítajte dĺžku tohoto mena a meno samotné. Potom vypíšte prianie pekného dňa
používateľovi s jeho celým menom. Použite dynamickú alokáciu pamäti.

#+BEGIN_EXAMPLE
Ukážka vstupu:
7
3
Jan
5
Josef
6
Václav
7
Antonín
9
František
5
Karel
7
Radecký
#+END_EXAMPLE

#+BEGIN_EXAMPLE
Ukážka výstupu:
Dobry den Jan Josef Václav Antonín František Karel Radecký!
#+END_EXAMPLE

** Smerník na funkciu
Predstavte si, že ste si naprogramovali funkciu, ktorá vám usporiadáva pole
čísiel vzostupne. Zrazu však prídete na to, že by ste ale potrebovali zoradiť aj
zostupne. Mohli by ste vytvoriť funkciu, čo usporiadáva zostupne, prekopírovaním
logiky, a len upravením spôsobu porovnávania prvkov. Ale to by nebol moc
programátorský prístup, lebo by sme duplikovali kód.

Čo môžeme spraviť, je povedať funkcií akým spôsobom má prvky porovnávať.
Teda pošleme jej funkciu /porovnaj/, ktorá ju abstrahuje od logiky porovnávania,
a naša usporadúvacia funkcia sa bude venovať len usporadúvaniu.

#+BEGIN_EXAMPLE c
void bubbleSort(int * array, int size, int (*compare)(int, int)){
   for(int i = 0; i < size - 1; i++){
       for(int j = 0; j < size - i - 1; j++){

           if(compare(array[j+1], array[j])){
               //volanie funkcie compare, ktora prisla ako argument

               int tmp = array[j + 1];
               array[j + 1] = array[j];
               array[j] = tmp;
           }   
       }   
   }   
}    

int cmp_ascending(int item1, int item2){
    return item1 < item2;
}

int cmp_descending(int item1, int item2){
    return item1 > item2;
}

int main {
    int* arr = {4,9,6};
    bubbleSort(arr, 3, cmp_ascending); // {4,6,9}
    bubbleSort(arr, 3, cmp_descending); // {9,6,4}

    return 0;
}
#+END_EXAMPLE

Vidíte, že šikovným poslaním funkcie, sme si ušetrili kopec práce s písaním kódu.
Zároveň nám to umožňuje kedykoľvek upraviť spôsob porovnávania prvkov, bez toho aby
sme menili funkciu usporadúvania a naopak.

** Úloha
*Aplikácia* - jednoduchý príklad na aplikáciu smerníka na funkciu.

Napíšte program, ktorý bude slúžiť ako jednoduchá kalkulačka, ktorá podporuje štyri
operácie: sčítanie, odčítanie, násobenie, delenie. Používateľ zadáva z klávesnice
znaky vybranej operácie: s – sčítanie, o – odčítanie, n – násobenie, d – delenie a
hodnoty operandov. Program sa ukončí po zadaní znaku e – exit. V prípade, ak
používateľ zadá iný ako predtým uvedené znaky, program vypíše oznam Operacia
nie je podporovana a čaká na zadanie ďalšej operácie. Každá operácia sa bude
vykonávať v samostatnej funkcii. Jednotlivé funkcie budú volané prostredníctvom
ukazovateľa na funkciu. V hlavnej funkcií sa nachádza práve 1 volanie inej funkcie.

#+BEGIN_EXAMPLE
Ukážka vstupu:
s↵
2 5↵
t↵
n↵
3 12↵
e↵
#+END_EXAMPLE

#+BEGIN_EXAMPLE
Ukážka výstupu:
7 ↵
Operacia nie je podporovana↵
36 ↵
#+END_EXAMPLE

* Osvojené zručnosti
Študenti sa naučili dynamicky alokovať pamäť pre jednoduché premenné,
jednorozmerné a viacrozmerné polia. Vedia adresu tejto pamäte ukladať do
smerníkov a následne k nej pristupovať a manipulovať s ňou podľa potreby.
Takisto vedia posielať ako argument a vyvolávať funkcie prostredníctvom
smerníka na funkciu.
* Očakávané chyby
- Študenti si mýlia operátory prístupu k hodnote a k adrese.
  - Treba zdôrazniť, čo znamená unárny operátor hviezdička a ampersand a kedy
    ktorý použiť.
- Študenti miešajú pri smerníkovej aritmetike poradie indexov.
  - Treba zdôrazniť, že indexy sa zapisujú štýlom FCFS, teda prvý index, ktorým
    sa vnáram, je ten najvnútornejší
* Didaktické zásady
** Zásada názornosti
Zásada názornosti je aplikovaná prekladaním výkladu s úlohami, ktoré okamžite
demonštrujú význam prednesenej látky.
** Zásada systematickosti a sústavnosti
Zásada sa uplatňuje tak, že sa objasní základný pojem smerníka a postupne sa
okolo neho buduje vzťažná sústava súvisiaca s jeho aplikáciami, pričom sa
postupuje od najjednoduchších smerníkov na premmené a postupne sa pridávajú iné
dynamické štruktúry a nakoniec funkcie.
** Zásada uvedomelosti a aktivity
Úlohy sú koncipované tak, že vyžadujú aplikovať práve prednesené znalosti,
pričom ich plnenie má prispieť k plnému pochopeniu povedaného.
