(TeX-add-style-hook
 "didakticky_vystup"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:orgc7fb137"
    "sec:orga2e9938"
    "sec:orgd55e73c"
    "sec:org28cfd17"
    "sec:orga3aa68b"
    "sec:org8ecdcf7"
    "sec:org201ad1e"
    "sec:org45ce43f"
    "fig:orge3de97e"
    "sec:org4acefed"
    "sec:org8d43f6f"
    "sec:orgd0c0793"
    "sec:org7097553"
    "sec:orge8f0326"
    "sec:org33c588c"
    "sec:orga76b5b1"
    "sec:org17bb709"
    "sec:org5da49e2"
    "sec:orgf0c7858"
    "sec:orgcf7ef8a"
    "sec:org15518be"
    "sec:org925d3de"
    "sec:orgbcfc0bb"
    "sec:orgc390976"
    "sec:org12ec4b9"
    "sec:org978ff9b"
    "sec:orgf0824f0"
    "sec:org77612c0"))
 :latex)

