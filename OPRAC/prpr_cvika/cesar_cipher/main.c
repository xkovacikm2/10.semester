#include <stdio.h>
#include <stdlib.h>
// f03 prednaska


void odelovac() {
    printf("\n-----------------------------------------------\n");
}

void vypis(FILE *f) {
    char c;

    while ((c = getc(f)) != EOF) {
        putchar(c);
    }

    rewind(f);
}

void vypis_cisel(FILE *f) {
    int hodnota = 0;
    char c;

    while ((c = getc(f)) != EOF) {

        while ((c = getc(f)) == '$');            /* precita  a ignoruje vsetky znaky $ */
        ungetc(c, f);  /* vrati prvu cislicu cisla do buffera*/
        fscanf(f, "%d", &hodnota);
        printf("\n%6d", hodnota);

    }
}

char sifruj(char znak, int posun) {

    if ((znak >= 'A') && (znak <= 'Z')) {
        if ((znak + posun) <= 'Z')
            return (znak + posun);
        else
            return (((znak + posun) % 90) + 'A' - 1);
    } else if ((znak >= 'a') && (znak <= 'z')) {
        if ((znak + posun) <= 'z')
            return (znak + posun);
        else
            return (((znak + posun) % 122) + 'a' - 1);
    } else
        return (znak);
}

char desifruj(char znak, int posun) {
    if ((znak >= 'A') && (znak <= 'Z')) {
        if ((znak - posun) >= 'A')
            return (znak - posun);
        else
            return ('Z' - ('A' - (znak - posun) - 1));
    } else if ((znak >= 'a') && (znak <= 'z')) {
        if ((znak - posun) >= 'a')
            return (znak - posun);
        else
            return ('z' - ('a' - (znak - posun) - 1));
    } else
        return (znak);
}

void Sifrovanie(FILE *fsorce, FILE *ftarget, int posun) {
    char c;
    while ((c = getc(fsorce)) != EOF) {
        fprintf(ftarget, "%c", sifruj(c, posun));
    }
    fprintf(ftarget, "%c", c);

}

void Desifrovanie(FILE *fsorce, FILE *ftarget, int posun) {
    char c;
    while ((c = getc(fsorce)) != EOF) {
        fprintf(ftarget, "%c", desifruj(c, posun));
    }
    fprintf(ftarget, "%c", c);
}


int main() {
    FILE *f, *fs, *fd, *fc;
    void (*ukf)(FILE *, FILE *, int);
    int posun = 0;
    char pole_znakov_stat[20];

    printf("\nZadaj cislo v intervale <0,25>: ");

    do {
        scanf(" %d", &posun);
    } while ((posun < 0) || (posun > 25));

    fflush(stdin);

    printf("\nZadaj vetu o velkosti 20 znakov: ");
    for (int i = 0; i < 20; i++) {
        scanf("%c", &pole_znakov_stat[i]);
    }

    odelovac();
    printf("Vypis znakov pola:");

    for (int i = 0; i < 20; i++)
        printf("%c", pole_znakov_stat[i]);

    // zapis znakov do suboru
    if ((f = fopen("VSTUP.txt", "w+")) == NULL) {
        printf("Nepodarilo sa otvorit subory.\n");
        return 1;
    }
    if ((fs = fopen("VSTUP_sifrovany", "w+")) == NULL) {
        printf("Nepodarilo sa otvorit subory.\n");
        return 1;
    }
    if ((fd = fopen("VSTUP_desifrovany.txt", "w+")) == NULL) {
        printf("Nepodarilo sa otvorit subory.\n");
        return 1;
    }
    if ((fc = fopen("subor_cislo.txt", "r")) == NULL) {
        printf("Nepodarilo sa otvorit subory.\n");
        return 1;
    }

    rewind(f);
    for (int i = 0; i < 20; i++)
        fprintf(f, "%c", pole_znakov_stat[i]);
    fprintf(f, "%c", EOF);

    // sifrovanie suboru
    rewind(f);
    rewind(fs);
    //Sifrovanie(f,fs,posun);
    ukf = Sifrovanie;
    (*ukf)(f, fs, posun);

    // desifrovanie suboru
    rewind(fs);
    rewind(fd);
    Desifrovanie(fs, fd, posun);

    // vypis suborov na obrazovku
    rewind(f);
    rewind(fs);
    rewind(fd);
    odelovac();
    printf("VSTUP.txt\n");
    vypis(f);
    odelovac();
    printf("VSTUP_sifrovany.txt\n");
    vypis(fs);
    odelovac();
    printf("VSTUP_desifrovany.txt\n");
    vypis(fd);

    // vypis cisel so suboru 'subor_cislo.txt'
    odelovac();
    vypis_cisel(fc);

    if (fclose(f) == EOF) {
        printf("Nepodarilo sa zatvorit subor.\n");
        return 1;
    }
    if (fclose(fs) == EOF) {
        printf("Nepodarilo sa zatvorit subor.\n");
        return 1;
    }
    if (fclose(fd) == EOF) {
        printf("Nepodarilo sa zatvorit subor.\n");
        return 1;
    }
    if (fclose(fc) == EOF) {
        printf("Nepodarilo sa zatvorit subor.\n");
        return 1;
    }

    fflush(stdin);
    getchar();
    return 0;
}
