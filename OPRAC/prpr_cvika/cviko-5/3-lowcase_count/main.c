#include <stdio.h>
#include <stdlib.h>

#define is_lower(x) (((x) >= 'a') && ((x) <= 'z'))

FILE *safely_open_file(char* filename, char* mode){
    FILE *f;

    if ((f = fopen(filename, mode)) == NULL){
        printf("File %s failed to open", filename);
        exit(1);
    }

    return f;
}

void safely_close_file(FILE **f){
    if (fclose(*f) == EOF){
        printf("File failed to close");
        exit(1);
    }
}

int main() {
    FILE *f_in, *f_out;
    char c;
    int lowcase_counter = 0;

    f_in = safely_open_file("vstup.txt", "r");
    f_out = safely_open_file("cisla.txt", "a+");

    while((c = getc(f_in)) != EOF) {
        putc(c, f_out);

        if (is_lower(c)){
            lowcase_counter++;
        }

        if (c == '\n'){
            fprintf(f_out, "%d\n", lowcase_counter);
            lowcase_counter = 0;
        }
    }

    safely_close_file(&f_in);
    safely_close_file(&f_out);

    return 0;
}