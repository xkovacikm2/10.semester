#include <stdio.h>
#include <stdlib.h>

FILE *safely_open_file(char* filename, char* mode){
    FILE *f;

    if ((f = fopen(filename, mode)) == NULL){
        printf("File %s failed to open", filename);
        exit(1);
    }

    return f;
}

void safely_close_file(FILE **f){
    if (fclose(*f) == EOF){
        printf("File failed to close");
        exit(1);
    }
}

int main() {
    FILE *f_in, *f_out;
    char c;
    char is_stdin = 0;

    f_in = safely_open_file("znak.txt", "r");

    if (getchar() == 's'){
        f_out = safely_open_file("novy.txt", "w+");
        is_stdin = 1;
    } else {
        f_out = stdout;
    }

    while ((c = getc(f_in)) != EOF){
        putc(c, f_out);
    }

    safely_close_file(&f_in);

    if(!is_stdin){
        safely_close_file(&f_out);
    }

    return 0;
}