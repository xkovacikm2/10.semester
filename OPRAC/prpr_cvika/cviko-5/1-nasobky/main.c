#include <stdio.h>

int main() {
    float n;
    int i;
    FILE* f;

    printf("Enter number: ");
    scanf("%f", &n);

    if ((f = fopen("nasobky.txt", "w+")) == NULL){
        printf("File failed to open");
    }

    for(i = 1; i <= 10; i++) {
        fprintf(f, "%d * %.2f = %.2f\n", i, n, n*i);
    }

    if ((fclose(f)) == EOF){
        printf("File failed to close");
    }

    return 0;
}