#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include <cv.hpp>

int main() {
    auto image = cv::imread("img/lena.bmp", cv::IMREAD_COLOR);
    cv::namedWindow("test", cv::WINDOW_AUTOSIZE);
    cv::imshow("test", image);

    cv::waitKey(0);
    return 0;
}