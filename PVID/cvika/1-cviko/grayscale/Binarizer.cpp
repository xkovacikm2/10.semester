//
// Created by Michal Kovačik on 2018/02/15.
//

#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv/cv.hpp>
#include "Binarizer.h"
#include "GrayScaler.h"

cv::Mat Binarizer::binarize(cv::Mat image) {
    cv::Mat grey_im;
    cv::threshold(image, grey_im, 127, 255, cv::THRESH_BINARY);

    return grey_im;
}
