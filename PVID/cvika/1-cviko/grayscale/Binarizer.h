//
// Created by Michal Kovačik on 2018/02/15.
//

#ifndef GRAYSCALE_BINARIZER_H
#define GRAYSCALE_BINARIZER_H


#include <string>

class Binarizer {
public:
    cv::Mat binarize(cv::Mat image);
};


#endif //GRAYSCALE_BINARIZER_H
