//
// Created by Michal Kovačik on 2018/02/15.
//

#ifndef GRAYSCALE_MORPHER_H
#define GRAYSCALE_MORPHER_H


#include <opencv2/core/mat.hpp>

class Morpher {
public:
    Morpher();
    cv::Mat dilatate(cv::Mat image);
    cv::Mat erode(cv::Mat image);

private:
    cv::Mat element;
};


#endif //GRAYSCALE_MORPHER_H
