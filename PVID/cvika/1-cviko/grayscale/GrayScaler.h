//
// Created by Michal Kovačik on 2018/02/15.
//

#ifndef GRAYSCALE_GRAY_SCALER_H
#define GRAYSCALE_GRAY_SCALER_H


class GrayScaler {
public:
    cv::Mat greyscale(cv::Mat image_name);
};


#endif //GRAYSCALE_GRAY_SCALER_H
