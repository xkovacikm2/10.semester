//
// Created by Michal Kovačik on 2018/02/15.
//

#include <opencv/cv.hpp>
#include "Morpher.h"

cv::Mat Morpher::dilatate(cv::Mat image) {
    cv::Mat dilatated;

    cv::dilate(image, dilatated, this->element);

    return dilatated;
}

cv::Mat Morpher::erode(cv::Mat image) {
    cv::Mat eroded;

    cv::erode(image, eroded, this->element);

    return eroded;
}

Morpher::Morpher() {
    this->element = cv::getStructuringElement(cv::MorphShapes::MORPH_CROSS, cv::Size(3,3));
}

