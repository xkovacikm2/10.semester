//
// Created by Michal Kovačik on 2018/02/15.
//

#include <opencv2/core/mat.hpp>
#include <opencv/cv.hpp>
#include "GrayScaler.h"

cv::Mat GrayScaler::greyscale(cv::Mat image) {
    cv::Mat grey_im;
    cv::cvtColor(image, grey_im, cv::COLOR_RGB2GRAY);

    return grey_im;
}
