#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include <opencv/cv.hpp>
#include "GrayScaler.h"
#include "Binarizer.h"
#include "Morpher.h"

int main() {
    auto image = cv::imread("img/lena.bmp");

    GrayScaler scaler;
    auto grey = scaler.greyscale(image);

    Binarizer binarizer;
    auto binary = binarizer.binarize(grey);

    Morpher morpher;
    auto dilated_value = morpher.dilatate(binary);
    auto eroded_value = morpher.erode(binary);

    cv::imshow("dilated", dilated_value);
    cv::imshow("eroded", eroded_value);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv:findContours(binary, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    for (int i = 0; i >= 0; i = hierarchy[i][0]) {
        cv::Scalar color(rand()&255, rand()&255, rand()&255);
        cv::drawContours(image, contours, i, color, cv::FILLED, 8);
    }

    cv::imshow("contours", image);

    cv::waitKey(0);
    return 0;
}