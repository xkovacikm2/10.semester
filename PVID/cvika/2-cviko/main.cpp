#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include <opencv/cv.hpp>

using namespace cv;
int main() {
    auto image = imread("img/lena.bmp");

    std::vector<Mat> channels;
    split(image, channels);

    {
        std::vector<Mat> swp_channels;
        for (const auto channel : channels){
            swp_channels.insert(swp_channels.begin(), channel);
        }

        Mat modded;
        merge(swp_channels, modded);

        imshow("", modded);
        waitKey(0);
    }

    {
        Mat f_mat;
        image.convertTo(f_mat, CV_32FC3, 1.0/255);

        auto point = f_mat.at<float>(1,1);

        std::cout << point;
    }

    return 0;
}