library(klaR)

train <- rbind(iris[2:50,], iris[52:100,], iris[102:150,])
test <- rbind(iris[1,], iris[51,], iris[101,])

naiveBayesModel <- NaiveBayes(Species ~ ., data = train)
pred <- predict(naiveBayesModel, test)

require(caTools)

leukemiaData <- read.csv('genes-leukemia.csv')

set.seed(42)
indices <- runif(nrow(leukemiaData))
train <- leukemiaData[indices < 0.9,]
test <- leukemiaData[indices >= 0.9,]

leukemiaModel <- NaiveBayes(CLASS ~ ., data = train)
pred <- predict(leukemiaModel, test)

write.csv(train, file = 'train_leukemia.csv')
write.csv(test, file = 'test_leukemia.csv')
