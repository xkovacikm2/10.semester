(TeX-add-style-hook
 "prednaska"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org02fe1dd"
    "sec:orgd167069"
    "sec:org88d2a73"
    "sec:org0632c36"
    "sec:orgda1e6a5"
    "sec:org6e28760"
    "sec:orgd082246"
    "sec:org2fe5f05"
    "sec:orgc4323bc"
    "sec:org7f552e2"
    "sec:org7e7ebd6"
    "sec:org4309389"
    "sec:org159f384"
    "sec:orgb89b396"
    "sec:org004576d"
    "sec:org22fc24d"))
 :latex)

